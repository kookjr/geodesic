= Geodesic --  Distance and bearing calculations over the Earth's surface

This package contains Geodesic, which calculates distance and bearing
over the Earth's surface.

Geodesic contains the following features:

* A Position class to encapsulate an Earth position (latitude and longitude)

* A method to calculate the distance between two positions using Haversine formula

* A method to calculate the distance between two positions using the Law of Cosines

* A method to calculate the initial bearing between two positions

* A method to calculate a position a given distance from another position along a bearing

All input parameters to these functions are Floats. Longitudes
are expected to be in the range of -180.0 to 180.0 decimal degrees,
latitudes in the range of -90.0 to 90.0 decimal degrees and bearings in
the range of 0.0 to 360.0 (where 0.0 is North). Distances are measured in
kilometers.

== Installation

Geodesic can be installed as a Gem from Gemcutter. If Gemcutter is not listed as
one of your sources, do this ONCE. You can check with the --list option first.

  gem sources --list
  gem sources -a http://gemcutter.org

Then, install.

  sudo gem install geodesic

== Sample Usage

  require 'rubygems'
  require 'geodesic'

  fremont    = Geodesic::Position.new(37.549531, -121.998711)
  farmington = Geodesic::Position.new(37.923482, -121.015263)

  d = Geodesic::dist_haversine(fremont.lat, fremont.lon, farmington.lat, farmington.lon)
  print "distance from Fremont to Farmington is ", d, " kilometers\n"

  b = Geodesic::bearing(fremont.lat, fremont.lon, farmington.lat, farmington.lon)
  print "bearing is ", b, " degrees\n"

  p = Geodesic::dest_position(fremont.lat, fremont.lon, b, d + 10)
  print "10 kilometers beyond farmington is ", p.lat, ", ", p.lon, "\n"

The output is:

  distance from Fremont to Farmington is 95.957632116596 kilometers
  bearing is 64.0206814526114 degrees
  10 kilometers beyond farmington is 37.9619800970259, -120.912203460738

== Development
=== Source Repository

Geodesic is currently hosted at gitlab. The gitlab web page is
http://gitlab.com/kookjr/geodesic. The public git clone URL is
 
* https://gitlab.com/kookjr/geodesic.git

=== Running the Test Suite

Unit tests can be run with the following command.

  ruby -Ilib test/tc_geodesic.rb
    or
  rake test

=== Issues and Bug Reports

Report and follow issue status at the project's GitLab site, on the Issues tab.

== Credits

The original author of the JavaScript implementation, Chris Veness,
can be found at:

  http://www.movable-type.co.uk/scripts/latlong.html

== License

GNU LGPL, Lesser General Public License version 2.1. For details,
see file "COPYING.LESSER".
