# geodesic - Latitude/longitude spherical geodesy formulae & scripts
#
# Copyright (C) Chris Veness 2002-2009
# Copyright (C) 2009 Mathew Cucuzella (kookjr@gmail.com)
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

# Additions to Float for conversion between various units.
class Float
  # Convert decimal degrees to radians. Input range is either -180.0 to 180.0
  # for longitudes, or -90.0 to 90.0 for latitudes.
  def to_radians
    self * Math::PI / 180.0
  end

  # Convert radians to degrees. Return range is -180.0 to 180.0.
  def to_degrees
    self * 180 / Math::PI
  end

  # Convert radians to bearing. Return degrees range 0.0 to 360.0.
  def to_bearing
    (self.to_degrees()+360.0) % 360.0
  end
end

# Geodesic is a Ruby library for calculating distance and bearing on
# the Earth's surface.  Positions are defined by longitude and
# latitude. The API uses Float values for all input and returned
# types. Distances are memasured in kilometers (km).
module Geodesic

  # Earth's radius in kilometers
  EARTH_RADIUS = 6371.0


  # Class to hold the latitude and longitude of a position.
  class Position
    attr_accessor :lat
    attr_accessor :lon

    # Construct a position with a given latitude and longitude.
    def initialize(lat, lon)
      @lat = lat
      @lon = lon
    end
  end

  # Use Haversine formula to Calculate distance (in kilometers) between two positions specified by 
  # latitude/longitude in numeric degrees of type Float.
  def Geodesic.dist_haversine(lat1, lon1, lat2, lon2)
    dLat = (lat2-lat1).to_radians()
    dLon = (lon2-lon1).to_radians()
    lat1 = lat1.to_radians()
    lat2 = lat2.to_radians()

    a = Math::sin(dLat/2) * Math::sin(dLat/2) +
      Math::cos(lat1) * Math::cos(lat2) * 
      Math::sin(dLon/2) * Math::sin(dLon/2)
    c = 2 * Math::atan2(Math::sqrt(a), Math::sqrt(1-a))
    d = EARTH_RADIUS * c
    return d
  end

  # Use Law of Cosines formula to calculate distance (in kilometers) between two positions specified by 
  # latitude/longitude in numeric degrees of type Float.
  def Geodesic.dist_cosine_law(lat1, lon1, lat2, lon2)
    d = Math::acos(Math::sin(lat1.to_radians())*Math::sin(lat2.to_radians()) +
                   Math::cos(lat1.to_radians())*Math::cos(lat2.to_radians()) *
                   Math::cos((lon2-lon1).to_radians())) * EARTH_RADIUS
    return d
  end

  # Calculate (initial) bearing in degrees between two positions. Return
  # range is 0.0 to 360.0. Latitudes and longitudes are of type Float.
  #   see http://williams.best.vwh.net/avform.htm#Crs
  def Geodesic.bearing(lat1, lon1, lat2, lon2)
    lat1 = lat1.to_radians()
    lat2 = lat2.to_radians()
    dLon = (lon2-lon1).to_radians()

    y = Math::sin(dLon) * Math::cos(lat2)
    x = Math::cos(lat1)*Math::sin(lat2) -
        Math::sin(lat1)*Math::cos(lat2)*Math::cos(dLon)
    return Math::atan2(y, x).to_bearing()
  end

  # Calculate destination position given start position, initial bearing (degrees 0.0 to 
  # 360.0) and distance (kilometers).
  # All values of type Float. Return a Position class or nil.
  #   see http://williams.best.vwh.net/avform.htm#LL
  def Geodesic.dest_position(lat1, lon1, brng, d)
    lat1 = lat1.to_radians()
    lon1 = lon1.to_radians()
    brng = brng.to_radians()

    lat2 = Math::asin(Math::sin(lat1)*Math::cos(d/EARTH_RADIUS) + 
                      Math::cos(lat1)*Math::sin(d/EARTH_RADIUS)*Math::cos(brng) )
    lon2 = lon1 + Math::atan2(Math::sin(brng)*Math::sin(d/EARTH_RADIUS)*Math::cos(lat1), 
                              Math::cos(d/EARTH_RADIUS)-Math::sin(lat1)*Math::sin(lat2))
    lon2 = (lon2+Math::PI)%(2*Math::PI) - Math::PI  # normalise to +/-radians

    return nil if lat2.nan? || lon2.nan?
    return Position.new(lat2.to_degrees(), lon2.to_degrees())
  end

end
