# -*- mode: ruby -*-

Gem::Specification.new do |s|
  s.name = 'geodesic'
  s.version = "1.0.1"
  s.summary = "Geodesic is a ruby library for calculating distance and bearing on the Earth's surface."
  s.description = "Geodesic is a Ruby library for calculating distance and bearing on the Earth\'s surface.  Positions are defined by longitude and latitude. The API uses Float values for all input and returned types. Distances are memasured in kilometers (km)."
  s.author = "Mathew Cucuzella"
  s.email = 'kookjr@gmail.com'
  s.homepage = 'http://github.com/kookjr/geodesic'
  s.files = ['lib/geodesic.rb', 'Changelog', 'COPYING.LESSER', 'README.rdoc']

  s.has_rdoc = true
  s.extra_rdoc_files = ['Changelog', 'COPYING.LESSER', 'README.rdoc']
  s.rdoc_options << "--line-numbers" << "--inline-source" <<
    '--main' << 'README.rdoc' <<
    '--title' << 'Geodesic Documenatation'
end
