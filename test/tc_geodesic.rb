# geodesic - Latitude/longitude spherical geodesy formulae & scripts
#
# Copyright (C) 2009 Mathew Cucuzella (kookjr@gmail.com)
# 
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

require 'geodesic'
require 'test/unit'

class TestGeodesic < Test::Unit::TestCase

  def test_floatAdds
    assert_equal(0.0, 0.0.to_radians.to_degrees);
    assert_equal(10.0, 10.0.to_radians.to_degrees);
    assert_equal(-10.0, -10.0.to_radians.to_degrees);

    assert_equal(0.0, 0.0.to_radians.to_bearing)
    assert_equal(90.0, 90.0.to_radians.to_bearing)
    assert_equal(270.0, -90.0.to_radians.to_bearing)
    assert_equal(180.0, 180.0.to_radians.to_bearing)
    assert_equal(180.0, -180.0.to_radians.to_bearing)
  end

  def test_position
    assert_equal(3.0, Geodesic::Position.new(3.0, 5.0).lat)
    assert_equal(5.0, Geodesic::Position.new(3.0, 5.0).lon)

    p = Geodesic::Position.new(3.0, 5.0)
    p.lat = 1.0
    p.lon = 2.0
    assert_equal(1.0, p.lat)
    assert_equal(2.0, p.lon)
  end

  LINE_1 = [46.38779349180244, -117.00718402862549, 46.387556676245254, -116.98602676391602]
  KM_PER_MILE = 1.609

  def test_dist_haversine
    klm = Geodesic::dist_haversine(LINE_1[0], LINE_1[1], LINE_1[2], LINE_1[3])
    # expect 1 mile, convert then mult by 10 to check we are at 1/10ths
    assert_equal(10, ((klm / KM_PER_MILE) * 10.0).round)
  end

  def test_dist_cosine_law
    klm = Geodesic::dist_cosine_law(LINE_1[0], LINE_1[1], LINE_1[2], LINE_1[3])
    # expect 1 mile, convert then mult by 10 to check we are at 1/10ths
    assert_equal(10, ((klm / KM_PER_MILE) * 10.0).round)
  end

  def test_bearing
    # test west to east
    degrees = Geodesic::bearing(LINE_1[0], LINE_1[1], LINE_1[2], LINE_1[3])
    assert_equal(91, degrees.round)
    # and east to west
    degrees = Geodesic::bearing(LINE_1[2], LINE_1[3], LINE_1[0], LINE_1[1])
    assert_equal(271, degrees.round)
  end

  def test_dest_position
    pos = Geodesic::dest_position(LINE_1[0], LINE_1[1], 90.922, KM_PER_MILE)
    assert_equal((LINE_1[0] * 10).round, (pos.lat * 10).round)
    assert_equal((LINE_1[1] * 10).round, (pos.lon * 10).round)
  end
end
